﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace DataGopi.Models
{
    public class Mall
    {
        [ScaffoldColumn(false)]
        [Key]
        public int MallId { get; set; }

        public string Name { get; set; }
        [Display(Name = "Entertainable")]
        public bool IsEntertainable { get; set; }

        [Display(Name = "PlayStore")]
        public bool IsPlayStore { get; set; }

        [Display(Name = "Theater")]
        public bool IsTheater { get; set; }

        
        [ScaffoldColumn(false)]
        public int LocationID { get; set; }
        public virtual Location Location { get; set; }


        public static List<Mall> ReadAllFromCSV(string filepath)
        {
            List<Mall> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => Mall.OneFromCsv(v))
                                         .ToList();
            return lst;
        }


        public static Mall OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Mall item = new Mall();

            int i = 0;
            item.Name = Convert.ToString(values[i++]);
            item.IsEntertainable = Convert.ToBoolean(values[i++]);
            item.IsPlayStore = Convert.ToBoolean(values[i++]);
            item.IsTheater = Convert.ToBoolean(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
    

