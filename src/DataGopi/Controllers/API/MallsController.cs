using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using DataGopi.Models;

namespace DataGopi.Controllers
{
    [Produces("application/json")]
    [Route("api/Malls")]
    public class MallsController : Controller
    {
        private AppDbContext _context;

        public MallsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Malls
        [HttpGet]
        public IEnumerable<Mall> GetMalls()
        {
            return _context.Malls;
        }

        // GET: api/Malls/5
        [HttpGet("{id}", Name = "GetMall")]
        public IActionResult GetMall([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Mall mall = _context.Malls.Single(m => m.MallId == id);

            if (mall == null)
            {
                return HttpNotFound();
            }

            return Ok(mall);
        }

        // PUT: api/Malls/5
        [HttpPut("{id}")]
        public IActionResult PutMall(int id, [FromBody] Mall mall)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != mall.MallId)
            {
                return HttpBadRequest();
            }

            _context.Entry(mall).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MallExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Malls
        [HttpPost]
        public IActionResult PostMall([FromBody] Mall mall)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Malls.Add(mall);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MallExists(mall.MallId))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetMall", new { id = mall.MallId }, mall);
        }

        // DELETE: api/Malls/5
        [HttpDelete("{id}")]
        public IActionResult DeleteMall(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Mall mall = _context.Malls.Single(m => m.MallId == id);
            if (mall == null)
            {
                return HttpNotFound();
            }

            _context.Malls.Remove(mall);
            _context.SaveChanges();

            return Ok(mall);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MallExists(int id)
        {
            return _context.Malls.Count(e => e.MallId == id) > 0;
        }
    }
}